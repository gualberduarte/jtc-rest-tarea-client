package py.com.jtc.rest.client.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import py.com.jtc.rest.client.bean.Empleado;

@Service
public class EmpleadoServiceImpl  implements EmpleadoService{
	private static List<Empleado> empleados;
	
	@Override
	public List<Empleado> obtenerEmpleados() {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Empleado[]> responseEntity = restTemplate.getForEntity("http://localhost:8090/empleados", Empleado[].class);
		empleados = Arrays.asList(responseEntity.getBody());
		return empleados;
	}
	
	@Override
	public Empleado obtenerEmpleado(Integer id) {
		//TODO: Pendiente de implementar cliente rest con RestTemplate
		return new Empleado();
	}

	@Override
	public void insertarEmpleado(Empleado empleado) {
		//TODO: pendiente
	}
	
	@Override
	public Empleado editarEmpleado(Integer id, Empleado empleado) {
		//TODO: Actualizar lo necesario en esta funcion
		return new Empleado();
	}
	
	@Override
	public void eliminarEmpleado(Integer id) {
		Map<String, Integer> params = new HashMap<String, Integer>();
	    params.put("id",id);
	    RestTemplate restTemplate = new RestTemplate();
	    restTemplate.delete("http://localhost:8090/empleados/{id}",params);
	}

}
